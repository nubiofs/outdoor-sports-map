require_relative '../../app/models/sensor.rb'
require_relative '../../app/models/tile_process.rb'

describe TileProcess do

  before :each do     
    data = [];
    data << DrawableSensor.new(22.0,5.0,5,6,1,18,1,2)
    data << DrawableSensor.new(22.0,5.0,5,6,1,18,1,2)
    data << DrawableSensor.new(22.0,5.0,5,6,1,18,1,2)
    @objTileProccess = TileProcess.new(data,1)
    @objPixel = PixelTile.new(1,2,'')
  end
 describe "#new" do
    it "takes three parameters and returns a TileProcess object" do        
        expect(@objPixel).to be_an_instance_of(PixelTile)
    end	
  end
  describe "#add_type" do
    it "add a type into the array types" do    
        @objPixel.add_type('type1')
        expect(@objPixel.types)==(['type1'])
    end	
  end
  describe "#include_type?" do
    it "returns true if the types array includes the type" do    
        @objPixel.add_type('type1')
        expect(@objPixel.include_type?('type1')).to be(true)
    end	
  end
 describe "#new" do
    it "takes one parameter and returns a TileProcess object" do        
        expect(@objTileProccess).to be_an_instance_of(TileProcess)
    end	
  end

 describe "#kilometres_to_pixel" do
 	it "converts kilometres to pixels" do
 		 expect(@objTileProccess.kilometres_to_pixel(10)).to eq(50)
 	end
  end

  describe "#degrees_to_pixel" do
 	it "converts degrees to pixels" do
 		 expect(@objTileProccess.degrees_to_pixel(10)).to eq(5009)
 	end
  end

  describe "#geographic_to_coordinates" do
 	it "converts from  a location of geographic coordinates to coordinates in the image" do
 		latlong = [20,20]
 		latlongi = [10,10]
 		 expect(@objTileProccess.geographic_to_coordinates(latlong,latlongi)).to eq([5009,-5009])
 	end
  end
  describe "#create_adjacency" do
  it "create a adjacency list" do
    @objTileProccess.create_adjacency(10)
    expect(@objTileProccess.adj_hash[10]).not_to be(nil)
  end
  end
  describe "#aleatory_string" do
  it "create a aleatory string" do
    expect(@objTileProccess.aleatory_string(2).size).to be(2)
  end
  end
 
  describe "#process" do
  it "create a aleatory string" do
    expect(@objTileProccess.process).not_to be(nil)
  end
  end 
end