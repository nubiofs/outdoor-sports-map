require 'spec_helper'

require_relative '../../app/models/sensor.rb'
require_relative '../../app/models/data_process.rb'
require 'json'
require 'rquad'
describe DataProcess do
  include RQuad
  before :each do
    @dataProcess = DataProcess.new
  end

  describe "#new" do
    it "returns a DataProcess object" do
      expect(@dataProcess).to be_an_instance_of(DataProcess)
      expect(@dataProcess.sensors)==[]
    end
  end

# file not found but on aplication are found 
  describe "#loadSensor" do
    it "load a data for sensors"  do
      @dataProcess.loadSensor nil
      expect(@dataProcess.sensors.size).not_to be(nil)
    end
  end

  describe "#processSensors" do
    it "process data from sensors"  do
      result=@dataProcess.processSensors 
      expect(result).not_to be(nil)
    end
  end
  
  json = '{"resources":[{"uuid":"85b00e2b-7dd4-41ac-bb5f-ce4f54853e21","lat":-23.522787,"lon":-46.490063,"collect_interval":60}]}'
  describe "#data_to_input_file" do
    it "correctly parses the required json for capacity temperature" do
      result=@dataProcess.data_to_input_file("Temperature",json)
      expect(result).not_to be(nil)
    end
  end
  
  describe "#data_to_input_file" do
    it "correctly parses the required json for capacity humidity" do
      result=@dataProcess.data_to_input_file("Humidity",json)
      expect(result).to be_an_instance_of(Array)
    end
  end
  
  describe "#data_to_input_file" do
    it "correctly parses the required json for capacity Pollution" do
      result=@dataProcess.data_to_input_file("Pollution",json)
      expect(result).not_to be(nil)
    end
  end
  
  describe "#data_to_input_file" do
    it "correctly parses the required json for capacity UV" do
      result=@dataProcess.data_to_input_file("UVs",json)
      expect(result).not_to be(nil)
    end
  end
  
  describe "#uuids" do
    it "correctly obtains an Array of the ids of a json" do
      result=@dataProcess.uuids(json)
      expect(result).to be_an_instance_of(Array)
    end
  end
end